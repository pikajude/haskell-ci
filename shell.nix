{ compiler ? "default", pkgs ? import <nixpkgs> {} }:

with pkgs;

let
  hp = if compiler == "default"
    then haskellPackages
    else haskell.packages.${compiler};

  isGhcjs = hp.ghc.isGhcjs or false;

  inHie = builtins.getEnv "HIE" == "1";

in with haskell.lib; hp.developPackage {
  root = ./.;
  modifier = drv: addBuildTool
    (addBuildDepend (dontCheck drv) hp.Cabal_3_0_0_0)
    hp.stylish-haskell;
  overrides = self: super: {
    base-compat = self.base-compat_0_11_0;
    time-compat = dontCheck super.time-compat;
    generic-lens = dontCheck (self.callHackageDirect {
      pkg = "generic-lens";
      ver = "1.2.0.1";
      sha256 = "0pkwyrmaj8wqlajb7cnswh7jq4pnvnhkjcl1flhw94gqn0vap50g";
    } {});
    HsYAML = self.callHackageDirect {
      pkg = "HsYAML";
      ver = "0.2.1.0";
      sha256 = "0r2034sw633npz7d2i4brljb5q1aham7kjz6r6vfxx8qqb23dwnc";
    } {};
  };
  source-overrides = {
    optparse-applicative = "0.15.0.0";
  };
}
