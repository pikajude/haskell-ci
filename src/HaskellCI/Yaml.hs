-- common functions that can be used to construct yaml trees
module HaskellCI.Yaml where

import           HaskellCI.Prelude

import qualified Data.List.NonEmpty      as NE

import           HaskellCI.Sh
import           HaskellCI.YamlSyntax

(~>) :: String -> Yaml [String] -> ([String], String, Yaml [String])
k ~> v = ([],k,v)

(^^^) :: ([String], String, Yaml [String]) -> String -> ([String], String, Yaml [String])
(a,b,c) ^^^ d = (d : a, b, c)

shListToYaml :: [Sh] -> Yaml [String]
shListToYaml shs = YList [] $ concat
    [ YString cs x : map fromString xs
    | (cs, x :| xs) <- gr shs
    ]
  where
    gr :: [Sh] -> [([String], NonEmpty String)]
    gr [] = []
    gr (Sh x : rest) = case gr rest of
        ([], xs) : xss -> ([], NE.cons x xs) : xss
        xss            -> ([], pure x) : xss

    gr (Comment c : rest) = case gr rest of
        (cs, xs) : xss -> (c : cs, xs) : xss
        []             -> [] -- end of comments are lost

ykeyValuesFilt :: ann -> [(ann, String, Yaml ann)] -> Yaml ann
ykeyValuesFilt ann xs = YKeyValues ann
    [ x
    | x@(_,_,y)  <- xs
    , not (isEmpty y)
    ]

ylistFilt :: ann -> [Yaml ann] -> Yaml ann
ylistFilt ann xs = YList ann
    [ x
    | x <- xs
    , not (isEmpty x)
    ]

isEmpty :: Yaml ann -> Bool
isEmpty (YList _ [])      = True
isEmpty (YKeyValues _ []) = True
isEmpty _                 = False
