{-# LANGUAGE RecordWildCards #-}

module HaskellCI.Circle.Yaml where

import           HaskellCI.Prelude

import           Distribution.Pretty            ( pretty )

import           HaskellCI.Yaml
import           HaskellCI.YamlSyntax

data Circle = Circle
    { circleVersion :: !Version
    }
  deriving Show

instance ToYaml Circle where
    toYaml Circle {..} = ykeyValuesFilt [] ["version" ~> fromString (show $ pretty circleVersion)]
