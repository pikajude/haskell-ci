{-# LANGUAGE RecordWildCards #-}

module HaskellCI.Circle where

import HaskellCI.Prelude

import HaskellCI.Circle.Yaml
import HaskellCI.Config
import HaskellCI.Jobs
import HaskellCI.Package
import HaskellCI.Project
import HaskellCI.Sh

makeCircle
    :: [String]
    -> Config
    -> Project Void Package
    -> JobVersions
    -> Either ShError Circle
makeCircle argv Config {..} prj JobVersions {..} = do
    return $ Circle { circleVersion = mkVersion [2,1] }
